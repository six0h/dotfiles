# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export TERM='xterm-256color'

#
## Plug for ZSH (Packages)
#
source ~/.zplug/init.zsh

zplug 'zplug/zplug', hook-build:'zplug --self-manage'
zplug 'robbyrussell/oh-my-zsh'
zplug "plugins/git", from:oh-my-zsh, if: "which git"
zplug "plugins/git-extras", from:oh-my-zsh, if:"which git"
zplug "plugins/tmuxinator", from:oh-my-zsh, if:"which tmux"
zplug "plugins/virtualenv", from:oh-my-zsh, if:"which virtualenv"
#zplug "TyWR/Nord-zsh", as:theme, depth:1
zplug "spaceship-prompt/spaceship-prompt", use:spaceship.zsh, from:github, as:theme
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "lib/history.zsh", from:oh-my-zsh
zplug "plugins/aws", from:oh-my-zsh, if: "which aws"
zplug "plugins/asdf", from:oh-my-zsh

zplug load

# Virtualenvwrapper things
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/projects
source /usr/bin/virtualenvwrapper.sh

# GoLang
export PATH=$PATH:/usr/local/go/bin

# Add Custom Scripts to PATH
export PATH=$PATH:$HOME/storage/cody/bin

# Add PHP Composer to PATH
export PATH=$PATH:$HOME/.config/composer/vendor/bin

# Add Pulumi to PATH
export PATH=$PATH:$HOME/.pulumi/bin

# Add cuda to PATH
export PATH="/opt/cuda-10.1/bin:$PATH"
export CPATH="/opt/cuda-10.1/include:$CPATH"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/cuda-10.1/lib64:/opt/cuda-10.1/lib"

# Add Serverless to PATH
export PATH="$HOME/.serverless/bin:$PATH"

# Ruby
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"

# TheFuck
eval $(thefuck --alias)

# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Keybinds
bindkey -e
bindkey "^[[3~" delete-char

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /home/code/projects/grizzlyforce/notification/node_modules/tabtab/.completions/serverless.zsh ]] && . /home/code/projects/grizzlyforce/notification/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /home/code/projects/grizzlyforce/notification/node_modules/tabtab/.completions/sls.zsh ]] && . /home/code/projects/grizzlyforce/notification/node_modules/tabtab/.completions/sls.zshsource /usr/share/nvm/init-nvm.sh

# opam configuration
[[ ! -r /home/code/.opam/opam-init/init.zsh ]] || source /home/code/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

# fzf activation
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git --ignore build --ignore dist --ignore .next --ignore node_modules --ignore .opam -f -g ""'
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# direnv
eval "$(direnv hook zsh)"

test -r "~/.dir_colors" && eval $(dircolors ~/.dir_colors)

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
