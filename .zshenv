alias ls='exa -lah'
alias vi='nvim'
alias vim='nvim'
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias vp='vagrant provision'
alias vu='vagrant up'
alias vd='vagrant destroy -f'
alias vs='vagrant ssh'
alias gitlog="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias mux='tmuxinator'
alias tl='tmux list-sessions'
alias tt='tmux attach -t $1'
alias tmux="TERM=screen-256color tmux"
alias yayclean='yay -Yc'
alias yayupdate='yay -Syu --devel --timeupdate'
alias drun='sudo docker run -it --network=host --device=/dev/kfd --device=/dev/dri --ipc=host --shm-size 16G --group-add video --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -v $HOME/dockerx:/dockerx rocm/tensorflow:latest'

export ANDROID_HOME=/mnt/storage/Cody/.Android/sdk
export AVD_HOME=/mnt/storage/Cody/.Android/avd
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:/home/cody/.gem/ruby/3.0.0/bin
export PATH=$PATH:/home/cody/go/bin

export VAGRANT_DEFAULT_PROVIDER='virtualbox'
export EDITOR='/usr/bin/nvim'
export VISUAL='/usr/bin/nvim'

export AWS_PROFILE='grizzly'
