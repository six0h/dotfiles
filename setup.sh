# ZPlug
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh

# Yay files
yay -S tmux vim tmux-plugin-manager
