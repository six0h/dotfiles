# Dotfiles
Cody Halovich
cody@sixohquad.com

## .vimrc
I have made sure that my .vimrc is well commented, so please read through it to figure out what its doing. If you have
any questions, don't hesitate to send me a message.

### Setup Instructions

1. First, setup Vundle as your plugin manager
2. Clone this repository
3. Symlink to the .vimrc in this repo
4. Open Vim, type the following
5. Restart vim, you're done!

~

    $ mkdir -p ~/.vim/bundle
    $ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    $ git clone https://six0h@bitbucket.org/six0h/dotfiles.git
    $ cd ~/
    $ ln -s dotfiles/.vimrc ~/.vimrc
    $ vi ~/.vimrc


In vim type: `:PluginInstall` and hit enter

### Add plugins
You can add your own plugins in the section that looks like this:

    Plugin 'rking/ag.vim'
    Plugin 'kien/ctrlp.vim'
    Plugin 'scrooloose/nerdtree'

All you have to do is head over to vimawesome.com, find a plugin you want, and drop in another 'Plugin' line for the
plugin you want. vimawesome provides simple instructions to get you going once you click on a plugin you're interested
in.

## .tmux.conf

### Install

1. You must install tmux plugin manager from https://github.com/tmux-plugins/tpm
2. symlink my .tmux.conf to your home folder
3. open tmux
4. ctrl + {actionkey}, I

## .ctags

### Install

1. You must install exuberant ctags (apt install exuberant-ctags, brew install ctags)
2. Place this file in your home directory

## .zshrc

This is the main ZSH config file

### Install

1. Install ZSH (apt install zsh, brew install zsh)
2. place this file in your home directory

## .zshenv

This file is where we store our environment variables

### Install

1. Install ZSH (apt install zsh, brew install zsh)
2. Place this in your home directory alongside your .zshrc
