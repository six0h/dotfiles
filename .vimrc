" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic Settings
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype off                   " required

set nowrap                     " do not wrap text
set number                     " show line numbers
set nocompatible               " be iMproved, required
set ruler                      " show character position in command line
set textwidth=0                " text width before wrap (if wrap is on)
set wrapmargin=0               " margin before wrap
set formatoptions=1
set history=500                " size of command history
set cursorline
set updatetime=300
set shortmess+=c
set background=dark


"set cursorcolumn

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keyboard Mappings
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Console Mappings
let g:mapleader = ','

" NERDTree Mappings
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeGitStatusUseNerdFonts = 1

" FZF Mappings
map <leader>f :FZF<CR>

" Fugitive
map <leader>g :Git<CR>
map <leader>gc :Git commit -m ""<CR>
map <leader>gp :Git push<CR>

" JSON Formatter
map <C-J> :%!python -m json.tool<CR>

" Tab Mappings
map <C-T>k :tabr<CR>
map <C-T>j :tabl<CR>
map <C-T>h :tabp<CR>
map <C-T>l :tabn<CR>

" Buffer Switching
map <leader>] :bn<CR>
map <leader>[ :bp<CR>

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors and File options
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax on                    " turn on syntax highlighting
set t_Co=256
set t_AB=^[[48;5;%dm
set t_AF=^[[38;5;%dm
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Set utf8 as standard encoding and en_US as the standard
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac
set nobackup
set nowb
set noswapfile

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Height of command line bar
set cmdheight=2
" set textwidth=120              " text width before wrap (if wrap is on)

" Be smart when using tabs ;)
set smarttab

" 1 tab == 1 tab 4 spaces wide
set shiftwidth=2
set tabstop=2
set softtabstop=2

" Linebreak on 500 characters

set autoindent "Auto indent
set smartindent "Smart indent

set rtp+=~/.fzf
filetype plugin indent on    " required

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Indent Guides
Plug 'nathanaelkane/vim-indent-guides'

" Nord Colorscheme
Plug 'arcticicestudio/nord-vim'

" Vim Eunuch - filesystem commands
Plug 'tpope/vim-eunuch'

" Vim Terraform
Plug 'hashivim/vim-terraform'

" CoC
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Beancount
Plug 'nathangrigg/vim-beancount'

" Language Server Protocol
"Plug 'mattn/vim-lsp-settings'
"Plug 'prabirshrestha/asyncomplete.vim'
"Plug 'prabirshrestha/asyncomplete-lsp.vim'

" Alignment
Plug 'junegunn/vim-easy-align'

" Git
Plug 'tpope/vim-fugitive'

" File tree
Plug 'scrooloose/nerdtree'

" Git Status for NERDTrek
Plug 'Xuyuanp/nerdtree-git-plugin'

" Pretty bar at the bottom
Plug 'vim-airline/vim-airline'

" Themes for pretty bar at th ebottom
Plug 'vim-airline/vim-airline-themes'

" Better indenting for Python
Plug 'vim-scripts/indentpython.vim'

" netrw network file editing
Plug 'vim-scripts/netrw.vim'

" fzf vim
Plug 'junegunn/fzf.vim'

" Editorconfig
Plug 'editorconfig/editorconfig-vim'

" Surroundings
Plug 'tpope/vim-surround'

" Markdown Preview
Plug 'JamshedVesuna/vim-markdown-preview'

" NNN
Plug 'mcchrish/nnn.vim'

" Goyo distraction free writing
Plug 'junegunn/goyo.vim'

" Initialize plugin system
call plug#end()

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin Settings
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indent
set ts=2 sw=2 et
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
" Airline
set laststatus=2
let g:airline_theme='nord'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" asyncomplete
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

" NERDTree
let g:NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" NERDTree Git
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

" Close Vim if NERDTree is only pane open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Open NERDTree automatically if no files are chosen
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Open NERDTree automatically when vim is opened on a directory
let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

"" Diff changes from saved file
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

" Rg
let g:rg_highlight = 1
map <leader>r :Rg<CR>

" ALE
let g:alex_fixers = {
      \  '*': ['remove_trailing_lines', 'trim_whitespace'],
      \'php': ['php_cs_fixer'],
      \}

let g:ale_fix_on_save = 1

" NNN Bindings
let g:nnn#layout = { 'window': { 'width': 0.5, 'height': 0.6, 'highlight': 'Debug' } }
let g:nnn#action = {
      \ '<c-t>': 'tab split',
      \ '<c-x>': 'split',
      \ '<c-v>': 'vsplit' }
let g:nnncommand = "nnn -de"

" terraform fmt on save
let g:terraform_fmt_on_save = 1

" goyo
function! s:goyo_enter() " quit on goyo quit
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave() " quit on goyo quit
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

autocmd! User GoyoEnter call <SID>goyo_enter()
autocmd! User GoyoLeave call <SID>goyo_leave()

" CoC
" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ CheckBackspace() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Language Specific Settings
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.* match BadWhitespace /^\t\+/
au BufRead,BufNewFile *.* match BadWhitespace /\s\+$/

"" Python Settings
let g:python_highlight_all=1
au BufRead,BufNewFile *.py,*.pyw set tabstop=4
au BufRead,BufNewFile *.py,*.pyw set softtabstop=4
au BufRead,BufNewFile *.py,*.pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw set autoindent
au         BufNewFile *.py,*.pyw set fileformat=unix
au BufRead,BufNewFile *.py,*.pyw let b:comment_leader = '#'

"" YAML Settings
au BufRead,BufNewFile *.yml,*.yaml,vagrant set tabstop=2
au BufRead,BufNewFile *.yml,*.yaml,vagrant set softtabstop=2
au BufRead,BufNewFile *.yml,*.yaml,vagrant set shiftwidth=2
au BufRead,BufNewFile *.yml,*.yaml,vagrant set autoindent
au         BufNewFile *.yml,*.yaml,vagrant set fileformat=unix
au BufRead,BufNewFile *.yml,*.yaml,vagrant let b:comment_leader = '#'

"" Terraform Settings
au BufRead,BufNewFile *.tf set tabstop=2
au BufRead,BufNewFile *.tf set softtabstop=2
au BufRead,BufNewFile *.tf set shiftwidth=2
au BufRead,BufNewFile *.tf set autoindent
au         BufNewFile *.tf set fileformat=unix
au BufRead,BufNewFile *.tf let b:comment_leader = '#'

"" Sass Settings
au BufRead,BufNewFile *.scss set tabstop=2
au BufRead,BufNewFile *.scss set softtabstop=2
au BufRead,BufNewFile *.scss set shiftwidth=2
au BufRead,BufNewFile *.scss set autoindent
au         BufNewFile *.scss set fileformat=unix
au BufRead,BufNewFile *.scss let b:comment_leader = '#'

"" JS Settings
au BufRead,BufNewFile *.js set tabstop=2
au BufRead,BufNewFile *.js set softtabstop=2
au BufRead,BufNewFile *.js set shiftwidth=2
au BufRead,BufNewFile *.js set autoindent
au         BufNewFile *.js set fileformat=unix
au BufRead,BufNewFile *.js let b:comment_leader = '#'

"" Makefile Settings
filetype plugin indent on
filetype detect
autocmd FileType make setlocal noexpandtab

" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Functions
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup
colorscheme nord
