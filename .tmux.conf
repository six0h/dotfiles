# Turn on Vi mode
setw -g mode-keys vi

# Turn mouse on
set-option -g mouse on

# Make Tmux use vim keys w/ wl-clipboard
set-option -s set-clipboard off
bind P paste-buffer
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi V send-keys -X rectangle-toggle
unbind -T copy-mode-vi Enter
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -se c -i'
bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel 'xclip -se c -i'
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xclip -se c -i'

# Setting the prefix from `C-b` to `C-a`.
# By remapping the `CapsLock` key to `Ctrl`,
# you can make triggering commands more comfottable!
set -g prefix C-a

# Free the original `Ctrl-b` prefix keybinding.
unbind C-b

# Ensure that we can send `Ctrl-a` to other apps.
bind C-a send-prefix

# Reload the file with Prefix r.
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# Splitting panes.
bind | split-window -h
bind - split-window -v

# Moving between panes.
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Moving between windows.
# Provided you've mapped your `CAPS LOCK` key to the `CTRL` key,
# you can now move between panes without moving your hands off the home row.
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# Pane resizing.
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

bind -r X kill-session

# Maximize and restore a pane.
unbind Up
bind Up new-window -d -n tmp \; swap-pane -s tmp.1 \; select-window -t tmp
unbind Down
bind Down last-window \; swap-pane -s tmp.1 \; kill-window -t tmp

# Setting the delay between prefix and command.
set -sg escape-time 1

# Set the base index for windows to 1 instead of 0.
set -g base-index 1

# Set the base index for panes to 1 instead of 0.
setw -g pane-base-index 1

# Set default shell to zsh
set-option -g default-shell /bin/zsh

# Colors
set -g default-terminal "xterm-256color"
set -ga terminal-overrides ",*256col*:Tc"

# Enable activity alerts.
setw -g monitor-activity on
set -g visual-activity on

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-resurrect'
# set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'jimeh/tmux-themepack'
set -g @plugin 'arcticicestudio/nord-tmux'

set -g @themepack 'powerline/default/nord'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
